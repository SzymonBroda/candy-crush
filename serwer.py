import socket
from _thread import *
import sys
import time


class Serwer(object):
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        self.currentId = "0"
        self.move = ['#', '#']
        self.map = 'brak mapy'
        self.player1_online = False
        self.player2_online = False
        self.hosted = False
        self.quit = ""
        while not self.hosted:
            try:
                self.try_host()
                self.hosted = True
            except:
                print("Not valid data, try again")

        self.run()

    def try_host(self):
        print("\nIf you want to start program localy, type 'localhost', suggested port: '5555'")
        self.server = input("IP ADRESS: ")
        self.port = int(input("PORT: "))
        self.server_ip = socket.gethostbyname(self.server)
        self.start()

    def start(self):
        try:
            self.s.bind((self.server, self.port))
        except socket.error as e:
            print(str(e))

        self.s.listen(2)
        print("Waiting for a connection")

    def threaded_client(self, conn, addr):
        if self.player1_online == False:
            self.player1_online = True
            playerID = "0"
        elif self.player2_online == False:
            self.player2_online = True
            playerID = "1"
        print("Connected to: ", addr, "ID", playerID)
        conn.send(str.encode(playerID))

        reply = ''
        while True:
            try:
                data = conn.recv(2048)
                reply = data.decode('utf-8')
                arr = reply.split(":")
                if self.quit == "QUIT":
                    conn.sendall(str.encode("QUIT"))

                if arr[0] == "0$":
                    self.map = arr[1]
                    conn.sendall(str.encode("map sent"))
                elif arr[0] == "1$":
                    conn.sendall(str.encode(str(self.map)))
                elif reply == 'QUIT':
                    self.quit = "QUIT"
                    break
                else:
                    id = int(arr[0])
                    self.move[id] += arr[1]
                    if id == 0: nid = 1
                    if id == 1: nid = 0
                    reply = self.move[nid][:]
                    self.move[nid] = '#'

                    conn.sendall(str.encode(reply))
            except:
                break
        if playerID == "0":
            self.player1_online = False
        elif playerID == "1":
            self.player2_online = False
        print("Connection Closed", addr, "ID", playerID)
        conn.close()

    def run(self):

        while True:
            conn, addr = self.s.accept()
            if self.player2_online == False and self.player1_online == False:
                self.quit = ''
            start_new_thread(self.threaded_client, (conn, addr))


server = Serwer()