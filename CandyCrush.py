from myUi import *
import sys
import numpy as np
import random
import time
from socket import AF_INET, socket, SOCK_STREAM, gethostname, error
from PyQt5 import QtCore, QtWidgets
from PyQt5.QtCore import Qt, QPoint, QPointF, QTimer
from PyQt5.QtWidgets import QGraphicsScene
from PyQt5.QtGui import QColor, QPen, QBrush, QPolygon, QPolygonF
import xml.etree.ElementTree as ET
from xml.dom import minidom
import json


OFFSET = 20
RADIUS = 8
SIZE = 2 * RADIUS
MAP_WIDTH = 20
MAP_HEIGHT = 15
COLORS = [Qt.cyan, Qt.red, Qt.blue, Qt.green, Qt.magenta, Qt.yellow, Qt.gray]


class Game(QtWidgets.QMainWindow):

    keyboard = QtCore.pyqtSignal(QtCore.QEvent)

    def __init__(self):
        QtWidgets.QWidget.__init__(self)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        self.init_game()
        self.init_button_connections()
        self.init_timers()


        self.replay_on = False
        self.player1_turn = True
        self.hot_seat_on = False
        self.online = False
        self.net = None
        self.show_instruction = False
        self.moves = ''
        self.enemy_moves = ''
        self.confirmed_data = False

        self.show()

    def init_game(self):

        self.ui.instruction.hide()
        self.ui.server.hide()
        self.ui.names.hide()
        self.ui.loadf.hide()
        self.ui.savef.hide()
        self.ui.replay_window.hide()
        self.player1 = Agent()
        self.player2 = Bot(self.player1)
        self.player1.beginning_map = self.player1.copy_to_str()
        self.ui.view1.setScene(self.player1.scene)
        self.ui.view2.setScene(self.player2.scene)

    def init_button_connections(self):
        self.keyboard.connect(self.on_key)
        self.ui.single.clicked.connect(self.single_mode)
        self.ui.hot_seat.clicked.connect(self.hot_seat_mode)
        self.ui.help.clicked.connect(self.show_help)
        self.ui.exit.clicked.connect(self.exit)
        self.ui.online.clicked.connect(self.online_mode)
        self.ui.confirm.clicked.connect(self.confirm_names)
        self.ui.confirm1.clicked.connect(self.connect)
        self.ui.save.clicked.connect(self.save_window)
        self.ui.load.clicked.connect(self.load_window)
        self.ui.save1.clicked.connect(self.save)
        self.ui.load1.clicked.connect(lambda: self.load(self.ui.load_name.text()))
        self.ui.cancel_s.clicked.connect(self.cancel)
        self.ui.cancel_l.clicked.connect(self.cancel)
        self.ui.cancel_sr.clicked.connect(self.cancel)
        self.ui.cancel_p.clicked.connect(self.cancel)
        self.ui.cancel_r.clicked.connect(self.cancel)
        self.ui.confirm_replay.clicked.connect(self.start_replay)
        self.ui.ai.clicked.connect(self.ai_mode)
        self.ui.replay.clicked.connect(self.replay_window)

    def init_timers(self):
        self.timer = QTimer()
        self.timer.setInterval(10)
        self.timer.timeout.connect(self.play_online)

        self.replay_timer = QTimer()
        self.replay_timer.setInterval(200)
        self.replay_timer.timeout.connect(self.replay)
        self.round_counter = 0

        self.bot_timer = QTimer()
        self.bot_timer.setInterval(300)
        self.bot_timer.timeout.connect(lambda: self.player2.action(self))

    def keyPressEvent(self, event):
        super(Game, self).keyPressEvent(event)
        self.keyboard.emit(event)

    def exit(self):
        self.close()

    def cancel(self):
        self.ui.server.hide()
        self.ui.names.hide()
        self.ui.loadf.hide()
        self.ui.savef.hide()
        self.ui.replay_window.hide()

    def show_help(self):
        self.stop_replay()
        self.show_instruction = not self.show_instruction
        if self.show_instruction:
            self.ui.instruction.show()
        else:
            self.ui.instruction.hide()

    def hide_player2_info(self):
        self.player2.scene.clear()
        self.ui.points2.setText("0")
        self.ui.agent_locked2.setText("")
        self.ui.name2.setText("")

    def show_player2_info(self):
        self.ui.points2.setText(str(self.player2.points))
        self.ui.points1.setText(str(self.player1.points))
        self.ui.label_2.setText("Points:")

    def confirm_names(self):
        self.player1.name = self.ui.player1.text()
        self.player2.name = self.ui.player2.text()
        self.ui.name1.setText(self.player1.name)
        self.ui.name2.setText(self.player2.name)
        if self.online:
            self.ui.name2.setText("Guest")
        self.ui.names.hide()

    def connect(self):
        host = self.ui.port.text()
        port = int(self.ui.ip_adress.text())
        self.ui.not_valid.setText("")

        try:
            self.net = Network(host, port)
            self.jsave_logs(host, port)
            self.restart()
            self.timer.start()
            self.ui.server.hide()
            self.online = True

            if self.net.id == '0':
                self.player1_turn = True
            else:
                self.player1_turn = False
                self.show_player2_info()
        except:
            self.ui.not_valid.setText("Connection error. Try again")

    def ai_mode(self):
        self.stop_replay()
        if self.online == True:
            self.disconnect()
        self.bot_timer.start()
        self.hot_seat_on = False
        self.online = False
        self.player2.bot_mode_on()
        self.restart()
        self.show_player2_info()

    def single_mode(self):
        self.hot_seat_on = False
        self.player2.bot = False
        self.online = False
        self.stop_replay()
        self.cancel()
        self.hide_player2_info()
        if self.online == True:
            self.disconnect()
        self.bot_timer.stop()
        self.restart()

    def hot_seat_mode(self):
        self.stop_replay()
        if self.online == True:
            self.disconnect()
        self.bot_timer.stop()
        self.hot_seat_on = True
        self.online = False
        self.player2.bot = False
        self.restart()
        self.show_player2_info()

    def online_mode(self):
        self.stop_replay()
        self.hot_seat_on = False
        self.player2.bot = False
        self.bot_timer.stop()
        if not self.online:
            host, port = self.jload_logs()
            self.ui.port.setText(port)
            self.ui.ip_adress.setText(host)
            self.ui.server.show()
            self.ui.savef.hide()
            self.ui.loadf.hide()
        else:
            self.disconnect()
            self.hide_player2_info()
            self.online = False

    def play_online(self):
        change_player = False
        exit = ''
        if self.player1_turn:
            exit = self.send_data(self.moves)
            self.moves = ''
        else:
            self.player1.locked = False
            self.enemy_moves = self.send_data(self.moves)
            self.moves = ''
            exit = self.enemy_moves
            moves = list(self.enemy_moves)
            for move in moves[1:]:
                change_player = self.player2.move_agent(move, False)
            self.check_locked()
            self.player2.scene.clear()
            self.player2.show_scene()
            self.ui.points2.setText(str(self.player2.points))
            self.enemy_moves = ''

        self.check_if_change_player(change_player)
        if exit == "QUIT":
            self.hide_player2_info()
            self.online = False

    def restart(self):
        self.player1_turn = True
        self.player1.restart_agent_pos()
        self.player2.restart_agent_pos()
        self.player1.points = 0
        self.player2.points = 0

        if  self.hot_seat_on:
            self.ui.player2.show()
            self.ui.player2.setText('player2')
        elif self.player2.bot:
            self.ui.player2.show()
            self.player2.name = "PC"
            self.ui.player2.setText(self.player2.name)
            self.show_player2_info()
        else:
            self.ui.player2.hide()
            self.ui.player2.setText('')
        self.ui.names.show()

        if self.online:
            if self.net.id == "0":
                self.player1.create_map(True)
                online_map = self.player1.prepare_map(True)
                self.net.send(str(self.net.id) + "$:" + online_map)
            else:
                map = self.net.send(str(self.net.id) + "$:" + "new map")
                self.player1.create_map(True, map)
        else:
            self.player1.create_map(True)
            self.player1.prepare_map()
        self.player1.beginning_map = self.player1.copy_to_str()
        self.player2.map = self.player1.copy_map()
        if self.hot_seat_on or self.online or self.player2.bot:
            self.player2.show_scene()
            self.ui.points2.setText(str(self.player2.points))
        self.player1.show_scene()
        self.ui.points1.setText(str(self.player1.points))

    def on_key(self, event):
        if not self.replay_on:
            change_player = False
            try:
                if not self.online:
                    if self.player1_turn or (not self.hot_seat_on and not self.player2.bot):
                        self.player2.locked = False
                        change_player = self.make_player_move(self.player1, chr(event.key()))
                        self.ui.points1.setText(str(self.player1.points))
                    elif not self.player2.bot:
                        self.player1.locked = False
                        change_player = self.make_player_move(self.player2, chr(event.key()))
                        self.ui.points2.setText(str(self.player2.points))
                else:
                    if self.player1_turn:
                        self.player2.locked = False
                        change_player = self.make_player_move(self.player1, chr(event.key()))
                        self.ui.points1.setText(str(self.player1.points))
                        self.moves += chr(event.key())

                self.check_if_change_player(change_player)
            except:
                pass #not valid key

    def check_if_change_player(self, change_player):
        if self.hot_seat_on or self.online or self.player2.bot:
            if change_player:
                self.player1_turn = not self.player1_turn
            if self.player1_turn:
                self.ui.turn.setText("<<<")
            else:
                self.ui.turn.setText(">>>")

    def make_player_move(self, player, event, replay=False):
        change_player = player.move_agent(event, replay)
        self.check_locked()
        player.scene.clear()
        player.show_scene()
        return change_player

    def check_locked(self):
        if self.player1.locked:
            self.ui.agent_locked1.setText("agent locked")
        else:
            self.ui.agent_locked1.setText("")
        if self.player2.locked:
            self.ui.agent_locked2.setText("agent locked")
        else:
            self.ui.agent_locked2.setText("")

    def send_data(self, data):
        data = str(self.net.id) + ":" + data
        reply = self.net.send(data)
        return reply

    def disconnect(self):
        self.online = False
        self.timer.stop()
        self.net.send("QUIT")

    def save_window(self):
        if not self.online:
            self.ui.savef.show()
            self.ui.loadf.hide()
            self.ui.replay_window.hide()
            self.stop_replay()
        else:
            print("can not save online game")

    def load_window(self):
        self.ui.loadf.show()
        self.ui.savef.hide()
        self.ui.replay_window.hide()
        self.stop_replay()

    def replay_window(self):
        self.ui.replay_window.show()
        self.ui.savef.hide()
        self.ui.loadf.hide()
        self.stop_replay()

    def jsave_mode(self):
        if self.hot_seat_on:
            mode = "hot_seat_mode"
        elif self.player2.bot:
            mode = "ai_mode"
        else:
            mode = "single_mode"
        return mode

    def jsave_turn(self):
        if self.player1_turn:
            turn = "player1"
        else:
            turn = "player2"
        return turn

    def jsave(self, name):
        jdata = {}
        jdata['player'] = []
        jdata['player'].append({
            'name': self.player1.name,
            'points': self.player1.points,
        })
        if self.online or self.hot_seat_on or self.player2.bot:
            jdata['player'].append({
                'name': self.player2.name,
                'points': self.player2.points,
            })
        jdata['mode'] = self.jsave_mode()
        jdata['turn'] = self.jsave_turn()

        with open('{}.txt'.format(name), 'w') as outfile:
            json.dump(jdata, outfile)

    def xmlsave(self, name):
        data = ET.Element('data')
        player1 = ET.SubElement(data, 'player')
        self.player1.player_data_to_xml(player1)
        if self.hot_seat_on or self.player2.bot:
            player2 = ET.SubElement(data, 'player')
            self.player2.player_data_to_xml(player2)

        mydata = ET.tostring(data)
        xmlstr = minidom.parseString(mydata).toprettyxml(indent="   ")
        f = open("{}.xml".format(name), "wb")
        f.write(xmlstr.encode('utf-8'))

    def save(self):
        name = self.ui.save_name.text()
        self.ui.savef.hide()
        try:
            self.jsave(name)
            self.xmlsave(name)
        except:
            print("save error")

    def jsave_logs(self, host, port):

         jdata = {}
         jdata['port'] = str(port)
         jdata['ip'] = host

         with open('log.txt', 'w') as outfile:
             json.dump(jdata, outfile)

    def jload_logs(self):
        with open('log.txt') as json_file:
            data = json.load(json_file)
            return str(data['port']), data['ip']

    def show_loaded_info(self):
        self.ui.name1.setText(self.player1.name)
        self.ui.points1.setText(str(self.player1.points))
        if self.hot_seat_on or self.player2.bot:
            self.show_player2_info()
            self.player2.show_scene()
            self.ui.name2.setText(self.player2.name)
            self.ui.points2.setText(str(self.player2.points))
        else:
            self.hide_player2_info()
        self.player1.show_scene()
        if self.player1_turn:
            self.ui.turn.setText("<<<")
        else:
            self.ui.turn.setText(">>>")
        self.update()

    def jload_mode(self, mode):
        if mode == 'single_mode':
            self.online = False
            self.hot_seat_on = False
            self.player2.bot = False
        elif mode == 'ai_mode':
            self.online = False
            self.hot_seat_on = False
            self.player2.bot = True
            self.player2.init_bot()
            self.player2.bot_mode_on()
            self.bot_timer.start()
        else:
            self.online = False
            self.hot_seat_on = True
            self.player2.bot = False

    def jload_turn(self, turn):
        if turn == "player1":
            self.player1_turn = True
        else:
            self.player1_turn = False

    def jload(self, name):
        with open('{}.txt'.format(name)) as json_file:
            data = json.load(json_file)
            self.jload_turn(data['turn'])
            self.jload_mode(data['mode'])
            self.player1.name = data['player'][0]['name']
            self.player1.points = int(data['player'][0]['points'])
            if self.online or self.hot_seat_on or self.player2.bot:
                self.player2.name = data['player'][1]['name']
                self.player2.points = int(data['player'][1]['points'])

    def xmlload(self, name):
        tree = ET.parse('{}.xml'.format(name))
        root = tree.getroot()
        players = root.findall('player')
        self.player1.moves_history = []
        self.player2.moves_history = []
        for elem in players[0].find('rounds'):
            self.player1.moves_history.append(list(elem.text))
        self.player1.create_map(True, players[0].find('map_at_end').text)
        self.player1.beginning_map = players[0].find('map_at_begin').text

        if self.hot_seat_on or self.player2.bot:
            self.player2.create_map(True, players[1].find('map_at_end').text)
            for elem in players[1].find('rounds'):
                self.player2.moves_history.append(list(elem.text))

    def load(self, name):
        self.ui.loadf.hide()
        if self.online:
            self.disconnect()
        try:
            self.jload(name)
            self.xmlload(name)
            self.player1.restart_agent_pos()
            self.player2.restart_agent_pos()
            self.show_loaded_info()
        except:
           print("load error")

    def stop_replay(self):
        self.replay_timer.stop()
        self.replay_on = False

    def start_replay(self):
        self.bot_timer.stop()
        name = self.ui.replay_name.text()
        self.ui.replay_window.hide()
        self.load(name)
        if not self.online:
            if not self.replay_on:
                self.player1.restart_agent_pos()
                self.player2.restart_agent_pos()
                self.player1.points = 0
                self.player2.points  = 0
                self.ui.points1.setText(str(self.player1.points))
                self.ui.points2.setText(str(self.player2.points))
                self.counter = 0
                self.replay_timer.start()
                self.replay_on = True
                self.player1.create_map(True, self.player1.beginning_map)
                if self.hot_seat_on or self.player2.bot:
                    self.player2.create_map(True, self.player1.beginning_map)
                    self.player2.show_scene()
                else:
                    self.hide_player2_info()
                self.player1.show_scene()
            else:
                self.stop_replay()

    def replay(self):
        if self.round_counter < len(self.player1.moves_history):
            if self.player1.move_counter < len(self.player1.moves_history[self.round_counter]):
                key = self.player1.moves_history[self.round_counter][self.player1.move_counter]
                self.ui.turn.setText("<<<")
                self.make_player_move(self.player1, key, True)
                self.ui.points1.setText(str(self.player1.points))
                self.player2.locked = False
                self.player1.move_counter +=1
            elif self.round_counter < len(self.player2.moves_history):
                if self.player2.move_counter < len(self.player2.moves_history[self.round_counter]):
                    key = self.player2.moves_history[self.round_counter][self.player2.move_counter]
                    self.ui.turn.setText(">>>")
                    self.make_player_move(self.player2, key, True)
                    self.ui.points2.setText(str(self.player2.points))
                    self.player1.locked = False
                    self.player2.move_counter += 1
                else:
                    self.round_counter += 1
                    self.player1.move_counter = 0
                    self.player2.move_counter = 0
            else:
                self.round_counter += 1
                self.player1.move_counter = 0
                self.player2.move_counter = 0

        elif self.round_counter >= len(self.player2.moves_history):
            self.replay_timer.stop()
            self.round_counter = 0
            self.player1.move_counter = 0
            self.player2.move_counter = 0
            self.replay_on = False
            if self.player2.bot:
                self.bot_timer.start()


class Elem(object):
    def __init__(self, color, x, y):
        self.color = color
        self.x = x
        self.y = y
        self.elem_shape = self.set_pointsF()

    def change_color(self, new_color):
        self.color = new_color

    def set_pointsF(self):
        row_correction = self.y % 2
        PointsF = QPolygonF([QPointF(- 8 + self.x * SIZE + row_correction * RADIUS, 5 + self.y * SIZE),
                  QPointF( self.x * SIZE + row_correction * RADIUS, 10 + self.y * SIZE),
                  QPointF( 8 + self.x * SIZE + row_correction * RADIUS, 5 + self.y * SIZE),
                  QPointF( 8 + self.x * SIZE + row_correction * RADIUS, - 5 + self.y * SIZE),
                  QPointF( self.x * SIZE + row_correction * RADIUS, - 10 + self.y * SIZE),
                  QPointF( - 8 + self.x * SIZE + row_correction * RADIUS, - 5 + self.y * SIZE)])
        return PointsF


class Map(object):

    def __init__(self, map=None):
        self.map = []
        self.next_color = 0
        self.combo_list = []
        self.your_turn = False
        self.beginning_map = ''
        if map is None:
            self.create_map()
            self.prepare_map()
        else:
            self.map = map.copy_map()
            self.next_color = map.next_color

    def create_map(self, new=False, map=None):
        if new:
            self.map = []
        #--------------------- kopiowanie online/ lub z pliku
        if map != None:
            map = list(map)
            self.next_color = int(map[0])
            for x in range(MAP_WIDTH):
                row = []
                for y in range(MAP_HEIGHT):
                    row.append(Elem(int(map[1 + x*MAP_HEIGHT + y]), x, y))
                self.map.append(row)
            self.map[0][0].choosen = True
            self.map = np.asarray(self.map)
        #-------------------- kopiowanie hot seat
        else:
            for x in range(MAP_WIDTH):
                row = []
                for y in range(MAP_HEIGHT):
                    color = random.randint(0, 6)
                    row.append(Elem(color, x, y))
                self.map.append(row)
            self.map[0][0].choosen = True
            self.map = np.asarray(self.map)

    def copy_map(self):
        map_copy = []
        for x in range(MAP_WIDTH):
            row = []
            for y in range(MAP_HEIGHT):
                row.append(Elem(self.map[x][y].color, x, y))
            map_copy.append(row)
        return (np.asarray(map_copy))

    def check_map(self):
        combo_list = []
        for i in range(1, self.map.shape[0]):
            for j in range(self.map.shape[1]):
                color = self.map[i][j].color

                # LEWO
                if i > 1:
                    elem1, elem2 = self.check_positions(i, j, -1, -2, 0, 0, color)
                    if elem1 is not None:
                        combo_list.append([i, j])
                        combo_list.append(elem1)
                        combo_list.append(elem2)
                '''if i < map.shape[0] - 2:
                    elem1, elem2 = check_positions(i, j, 1, 2, 0, 0, color, map)
                    if elem1 is not None:
                        combo_list.append(elem1)
                        combo_list.append(elem2)'''

                correction = j % 2
                '''if i < map.shape[0] - 1:
                    #PRAWO W DOL
                    if j < map.shape[1] - 2:
                        elem1, elem2 = check_positions(i, j, 0 + correction, 1, 1, 2, color, map)
                        if elem1 is not None:
                            combo_list.append(elem1)
                            combo_list.append(elem2)
                    #PRAWO W GORE
                    if j > 1:
                        elem1, elem2 = check_positions(i, j, 0 + correction, 1, -1, -2, color, map)
                        if elem1 is not None:
                            combo_list.append(elem1)
                            combo_list.append(elem2)'''

                # LEWO W DOL
                if j < self.map.shape[1] - 2:
                    elem1, elem2 = self.check_positions(i, j, -1 + correction, -1, 1, 2, color)
                    if elem1 is not None:
                        combo_list.append([i, j])
                        combo_list.append(elem1)
                        combo_list.append(elem2)
                # LEWO W GORE
                if j > 1:
                    elem1, elem2 = self.check_positions(i, j, -1 + correction, -1, -1, -2, color)
                    if elem1 is not None:
                        combo_list.append([i, j])
                        combo_list.append(elem1)
                        combo_list.append(elem2)

        if len(combo_list) != 0:
            combo_list = np.unique(combo_list, axis=0)
            combo_list = combo_list[combo_list[:, 1].argsort()]

        self.combo_list = combo_list

    def check_positions(self, x, y, x1, x2, y1, y2, color):
        x_1 = x + x1
        x_2 = x + x2
        y_1 = y + y1
        y_2 = y + y2
        if color == self.map[x_1][y_1].color:
            if color == self.map[x_2][y_2].color:
                return [x_1, y_1], [x_2, y_2]
        return None, None

    def prepare_map(self, online=False):
        self.check_map()
        while len(self.combo_list) != 0:
            self.make_changes()
            self.check_map()
        if online:
            return self.copy_to_str()

    def copy_to_str(self):
        map_online = str(self.next_color)
        for x in range(MAP_WIDTH):
            for y in range(MAP_HEIGHT):
                map_online += str(self.map[x][y].color)
        return map_online

    def make_changes(self):
        for elem in self.combo_list:
            self.map[elem[0]][elem[1]].change_color(self.next_color)
            self.next_color = (self.next_color + 1) % 7

    def drop_down(self):
        for elem in self.combo_list:
            for y in range(elem[1]):
                self.map[elem[0]][elem[1] - y].change_color(self.map[elem[0]][elem[1] - y - 1].color)
            self.map[elem[0]][0].change_color(self.next_color)
            self.next_color = (self.next_color + 1) % 7
        self.points += len(self.combo_list)


class Agent(Map):
    def __init__(self, map=None, x=0, y=0):
        super().__init__(map)
        self.scene = QGraphicsScene()
        self.x_pos = x
        self.y_pos = y
        self.locked = False
        self.points = 0
        self.agent_shape = self.set_agent_points()
        self.moves_history = []
        self.turn_history = []
        self.name = ''
        self.move_counter = 0

    def set_agent_points(self):
        row_correction = self.y_pos % 2
        Points = QPolygon([QPoint(OFFSET - 8 + self.x_pos * SIZE + row_correction * RADIUS, OFFSET + 5 + self.y_pos * SIZE),
                           QPoint(OFFSET + self.x_pos * SIZE + row_correction * RADIUS, OFFSET + 10 + self.y_pos * SIZE),
                           QPoint(OFFSET + 8 + self.x_pos * SIZE + row_correction * RADIUS, OFFSET + 5 + self.y_pos * SIZE),
                           QPoint(OFFSET + 8 + self.x_pos * SIZE + row_correction * RADIUS, OFFSET - 5 + self.y_pos * SIZE),
                           QPoint(OFFSET + self.x_pos * SIZE + row_correction * RADIUS, OFFSET - 10 + self.y_pos * SIZE),
                           QPoint(OFFSET - 8 + self.x_pos * SIZE + row_correction * RADIUS, OFFSET - 5 + self.y_pos * SIZE)])
        return Points

    def next_candy(self, key):
        if key == 'LUP':
            if self.y_pos > 0:
                if self.y_pos % 2 == 0:
                    if self.x_pos > 0:
                        self.x_pos -= 1
                        self.y_pos -= 1
                else:
                    self.y_pos -= 1
        elif key == 'RUP':
            if self.y_pos > 0:
                if self.y_pos % 2 == 1:
                    if self.x_pos < MAP_WIDTH - 1:
                        self.x_pos += 1
                        self.y_pos -= 1
                else:
                    self.y_pos -= 1

        elif key == 'L':
            if self.x_pos > 0:
                self.x_pos -= 1
        elif key == 'R':
            if self.x_pos < MAP_WIDTH - 1:
                self.x_pos += 1
        elif key == 'LDOWN':
            if self.y_pos < MAP_HEIGHT - 1:
                if self.y_pos % 2 == 0:
                    if self.x_pos > 0:
                        self.x_pos -= 1
                        self.y_pos += 1
                else:
                    self.y_pos += 1
        elif key == 'RDOWN':
            if self.y_pos < MAP_HEIGHT - 1:
                if self.y_pos % 2 == 1:
                    if self.x_pos < MAP_WIDTH - 1:
                        self.x_pos += 1
                        self.y_pos += 1
                else:
                    self.y_pos += 1

    def lock_position(self):
        self.locked = not self.locked

    def rotate_elements(self, key, map):
        x_change, y_change = 0, 0

        if key == 'LUP':
            if self.y_pos > 0:
                if self.y_pos % 2 == 0 and self.x_pos > 0:
                    x_change, y_change = -1, -1
                else:
                    x_change, y_change = 0, -1
        elif key == 'RUP':
            if self.x_pos < MAP_WIDTH - 1 and self.y_pos > 0:
                if self.y_pos % 2 == 1:
                    x_change, y_change = 1, -1
                else:
                    x_change, y_change = 0, -1
        elif key == 'L':
            if self.x_pos > 0:
                x_change, y_change = -1, 0
        elif key == 'R':
            if self.x_pos < MAP_WIDTH - 1:
                x_change, y_change = 1, 0
        elif key == 'LDOWN':
            if self.y_pos < MAP_HEIGHT - 1:
                if self.y_pos % 2 == 0 and self.x_pos > 0:
                    x_change, y_change = -1, 1
                else:
                    x_change, y_change = 0, 1
        elif key == 'RDOWN':
            if self.y_pos < MAP_HEIGHT - 1:
                if self.y_pos % 2 == 1 and self.x_pos < MAP_WIDTH - 1:
                    x_change, y_change = 1, 1
                else:
                    x_change, y_change = 0, 1

        if not (x_change == 0 and y_change == 0):
            self.locked = False
            map[self.x_pos, self.y_pos].color, map[self.x_pos +  x_change, self.y_pos + y_change].color \
                = map[self.x_pos +  x_change, self.y_pos + y_change].color,  map[self.x_pos, self.y_pos].color

    def move_agent(self, key, replay=False):
        command = None
        correct_key = False
        if key == 'W':
            correct_key = True
            command = 'LUP'
        elif key == 'E':
            correct_key = True
            command = 'RUP'
        elif key == 'A':
            correct_key = True
            command = 'L'
        elif key == 'D':
            correct_key = True
            command = 'R'
        elif key == 'Z':
            correct_key = True
            command = 'LDOWN'
        elif key == 'X':
            correct_key = True
            command = 'RDOWN'

        if correct_key and not replay:
            self.turn_history.append(key)

        if key == 'S':
            self.lock_position()
            if not replay:
                self.turn_history.append(key)
        elif correct_key:
            if self.locked is not True:
                self.next_candy(command)
                return False
            else:
                self.rotate_elements(command, self.map)
                self.next_candy(command)
                self.check_map()
                if not replay:
                    self.append_turn()
                while len(self.combo_list) != 0:
                    self.drop_down()
                    self.check_map()
                return True

    def show_scene(self):
        agent_elem = None
        for row in self.map:
            for elem in row:
                if elem.x == self.x_pos and elem.y == self.y_pos:
                    agent_elem = elem
                self.scene.addPolygon(elem.elem_shape, QPen(Qt.black,2), QBrush(COLORS[elem.color]))
        self.scene.addPolygon(agent_elem.elem_shape, QPen(Qt.white, 2), QBrush(COLORS[agent_elem.color]))

    def append_turn(self):
        self.moves_history.append(self.turn_history)
        self.turn_history = []

    def player_data_to_xml(self, player):
        # map
        last_map_elem = ET.SubElement(player, 'map_at_end')
        last_map_elem.text = (self.copy_to_str())
        begin_map_elem = ET.SubElement(player, 'map_at_begin')
        begin_map_elem.text = (self.beginning_map)
        #rounds
        rounds_elem = ET.SubElement(player, 'rounds')
        for i in range(len(self.moves_history)):
            round_elem = ET.SubElement(rounds_elem, 'round{}'.format(i))
            round = self.moves_history[i]
            text = ''
            for j in round:
                text += "{}".format(j)
            round_elem.text = text

    def restart_agent_pos(self):
        self.x_pos = 0
        self.y_pos = 0


class Bot(Agent):
    def __init__(self, map=None, x=0, y=0):
        super().__init__(map,x,y)
        self.ACTIONS = ['W','A','Z','X','D','E']
        self.horizontaly = True
        self.direction_flag = True
        self.aim = [int(MAP_WIDTH/2), MAP_HEIGHT - 2]
        self.rot_move = None
        self.first_moves = True
        self.done = False
        self.bot = False

    def init_bot(self):
        self.aim = [int(MAP_WIDTH/2), MAP_HEIGHT - 2]
        self.rot_move = None
        self.first_moves = True
        self.done = False
        self.horizontaly = True
        self.direction_flag = True

    def run_algorithm(self):
        self.done = False
        self.aim = []
        for i in range(self.x_pos - 3, self.x_pos + 3):
            for j in range(self.y_pos - 3, self.y_pos + 3):
                if i > 0 and j > 0 and i < self.map.shape[0]-1 and j < self.map.shape[1]-1 and not self.done:
                    correction = j % 2
                    # LEWO
                    if i > 1:
                        color = self.map[i-1][j].color
                        elem2 = self.check_position(i, j, -2, 0, color)
                        if elem2 is not None:
                            options = ['LUP', 'LDOWN', 'R', 'RDOWN', 'RUP']
                            self.done = self.check_around(i,j,color, options)
                    #PRAWO
                    elif i < self.map.shape[0] - 2:
                        color = self.map[i+1][j].color
                        elem2 = self.check_position(i, j, 2, 0, color)
                        if elem2 is not None:
                            options = ['LUP', 'LDOWN', 'L', 'RDOWN', 'RUP']
                            self.done = self.check_around(i, j, color, options)

                    if i < self.map.shape[0] - 2 and not self.done:
                        #PRAWO W DOL
                        if j < self.map.shape[1] - 2:
                            color = self.map[i + correction][j+1].color
                            elem2 = self.check_position(i, j, 1, 2, color)
                            if elem2 is not None:
                                options = ['LUP', 'LDOWN', 'L', 'R', 'RUP']
                                self.done = self.check_around(i, j, color, options)
                        #PRAWO W GORE
                        if j > 1 and not self.done:
                            color = self.map[i + correction][j - 1].color
                            elem2 = self.check_position(i, j, 1, -2, color)
                            if elem2 is not None:
                                options = ['LUP', 'LDOWN', 'L', 'R', 'RDOWN']
                                self.done = self.check_around(i, j, color, options)
                    if i > 1 and not self.done:
                        # LEWO W DOL
                        if j < self.map.shape[1] - 2:
                            color = self.map[i -1 + correction][j + 1].color
                            elem2 = self.check_position(i, j, -1, + 2, color)
                            if elem2 is not None:
                                options = ['LUP', 'RUP', 'L', 'R', 'RDOWN']
                                self.done = self.check_around(i, j, color, options)
                        # LEWO W GORE
                        if j > 1 and not self.done:
                            color = self.map[i - 1 + correction][j -1].color
                            elem2 = self.check_position(i, j, -1, -2, color)
                            if elem2 is not None:
                                options = ['LDOWN', 'RUP', 'L', 'R', 'RDOWN']
                                self.done = self.check_around(i, j, color, options)
                if self.done:
                    self.aim = [i,j]
                    break
            if self.done:
                break

    def check_around(self, x, y, color, options):
        correction = y % 2
        if 'LUP' in options:
            if self.check_position(x, y, -1 + correction, -1, color):
                self.rot_move = 'W'
                return True
        if 'L' in options:
            if self.check_position(x, y, -1, 0, color):
                self.rot_move = 'A'
                return True
        if 'LDOWN'in options:
            if self.check_position(x, y, -1 + correction, 1, color):
                self.rot_move = 'Z'
                return True
        if 'R' in options:
            if self.check_position(x, y, 1, 0, color):
                self.rot_move = 'D'
                return True
        if 'RUP' in options:
            if self.check_position(x, y, 0 + correction, -1, color):
                self.rot_move = 'E'
                return True
        if 'RDOWN' in options:
            if self.check_position(x, y, 0 + correction, 1, color):
                self.rot_move = 'X'
                return True
        return False

    def check_position(self, x, y, x1, y1, color):
        x_1 = x + x1
        y_1 = y + y1
        if color == self.map[x_1][y_1].color:
                return [x_1, y_1]
        return None

    def move_to_point(self, ui):
        if self.x_pos != self.aim[0] or self.y_pos != self.aim[1]:
            if self.horizontaly:
                if self.aim[0] > self.x_pos:
                    self.move_agent("D")
                elif self.aim[0] < self.x_pos:
                    self.move_agent("A")
                else:
                    self.horizontaly = False
            else:
                if self.aim[1] > self.y_pos:
                    if self.direction_flag:
                        self.move_agent('Z')
                    else:
                        self.move_agent('X')
                elif self.aim[1] < self.y_pos:
                    if self.direction_flag:
                        self.move_agent('W')
                    else:
                        self.move_agent('E')
                else:
                    self.horizontaly = True
                self.direction_flag = not self.direction_flag
        if self.x_pos == self.aim[0] and  self.y_pos == self.aim[1]:
            self.done = False
            if not self.first_moves:
                self.move_agent('S')
                self.move_agent(self.rot_move)
                ui.player1_turn = True
                ui.ui.points2.setText(str(self.points))
                ui.ui.turn.setText("<<<")
                self.first_moves = True
                self.aim = [int(MAP_WIDTH/2), MAP_HEIGHT - 2]
            else:
                self.first_moves = False
                self.aim = []

    def random_action(self):
        key = self.ACTIONS[random.randint(0,5)]
        self.move_agent(key, False)
        self.move_agent(key, False)

    def action(self,ui):
        if self.bot:
            if not ui.player1_turn:
                if self.first_moves:
                    self.move_to_point(ui)
                    self.done = False
                elif not self.done:
                    self.random_action()
                    self.run_algorithm()
                else:
                    self.move_to_point(ui)
                ui.player2.scene.clear()
                ui.player2.show_scene()

    def bot_mode_on(self):
        self.first_moves = True
        self.bot = True


class Network(object):

    def __init__(self, host, port):
        self.client = socket(AF_INET, SOCK_STREAM)
        self.host = host
        self.port = port
        self.addr = (self.host, self.port)
        self.id = self.connect()

    def connect(self):
        self.client.connect(self.addr)
        return self.client.recv(2048).decode()

    def send(self, data):
        try:
            self.client.send(str.encode(data))
            reply = self.client.recv(2048).decode()
            return reply
        except error as e:
            return str(e)


app = QtWidgets.QApplication(sys.argv)
MainWindow = Game()
sys.exit(app.exec_())

